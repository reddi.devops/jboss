FROM registry.access.redhat.com/ubi9/ubi:9.3-1610
WORKDIR /opt/
ENV hostname `hostname -f`
#ENV user jbossuser
#ENV password jbossuser
#ENV group guest
RUN echo $hostname
RUN yum install java-11-openjdk-devel wget unzip -y && \
ln -sf "/usr/share/zoneinfo/Asia/Kolkata" /etc/localtime  && \
wget https://gitlab.com/reddi.devops/jboss/-/raw/main/xaa && \
wget https://gitlab.com/reddi.devops/jboss/-/raw/main/xab && \
wget https://gitlab.com/reddi.devops/jboss/-/raw/main/xac && \
cat xa* > jboss-eap-8.0.zip && \
unzip jboss-eap-8.0.zip && \ 
mv /opt/jboss-eap-8.0/standalone/configuration/standalone-full-ha.xml /opt/jboss-eap-8.0/standalone/configuration/standalone.xml && \
sed -i 's/127.0.0.1/${hostname}/g' /opt/jboss-eap-8.0/standalone/configuration/standalone.xml && \
/opt/jboss-eap-8.0/bin/add-user.sh -u JbossTestUser -p JbossTestUser -g admin && \
ls -ltr /opt/jboss-eap-8.0/standalone/configuration && \
rm -rf jboss-eap-8.0.zip xaa xab xac && \
chmod 777 jboss-eap-8.0
WORKDIR /opt/jboss-eap-8.0/bin
RUN chmod 777 standalone.sh
EXPOSE 8080
EXPOSE 9990
EXPOSE 8443
CMD ["/opt/jboss-eap-8.0/bin/standalone.sh"]                                                                           

